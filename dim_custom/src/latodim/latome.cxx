#include <iostream>
#include <unistd.h>
#include <stdio.h>
#include <sstream>
#include <map>
#include <vector>
#include <fstream>
#include <string>
#include <bitset>

#include "latodim/latome.h"

template <typename T>
string binary_repr(T value) {
    // The bitset size is set to match the type size.
    return std::bitset<sizeof(T)*8>(value).to_string();
}


Latome::Latome(){ m_exist = false; }

Latome::Latome(int n, string sname)
{
    m_exist = true;
    set_number(n);
    set_servicename(sname);
    setup_ipbus();
}

uint32_t Latome::checkfirmware()
{
    if(!ping()){ return 0; }
    //the register name that we want to read
    //cout << "LATOME n" << m_servicename << "  le registre magique : " << ReadRegisterValue("fpga.firmware_name_0") << endl;
    return ReadRegisterValue("lli.remote_update.status.remote_update_id");
}

int Latome::checkbitslip(uint32_t* bitslip_err)
{
    if(!ping()){ return 1; }
    if(checkfirmware() != 0xBB){ return 0; }
    //if(ReadRegisterValue("fpga.firmware_name_0") == 1601073005){ return 0; }
    if(ReadRegisterValue("fpga.firmware_name_0") != 1634497633){ return 0; }
    //cout << "Kaaris a dit : " << ReadRegisterValue("fpga.firmware_name_0") << " pour la latome : " << m_servicename << endl;

    for (int fib = 1; fib < 48; fib++)
    {
        //word of 32 and 8 bit relation to check in the future
        const uint32_t value = ReadRegisterValue("istage.decode_stage_" + to_string(fib) + ".status.bitslip_error.value");
        *(bitslip_err + fib) = value;
    }
    return 0;
}

string Latome::get_textnumber()
{
    if (m_exist){ return m_snumber; }
    else{ return "NULL"; }
}
