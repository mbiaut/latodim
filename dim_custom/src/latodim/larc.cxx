#include <iostream>
#include <unistd.h>
#include <stdio.h>
#include <sstream>
#include <map>
#include <vector>
#include <fstream>
#include <string>

#include "latodim/larc.h"


void LArC::set_latome1(Latome n)
{
    m_latome1 = n;
    m_latome1.set_larc_textnumber(m_snumber);
    m_latome1.set_larc_servicename(m_servicename);
}

void LArC::set_latome2(Latome n)
{
    m_latome2 = n;
    m_latome2.set_larc_textnumber(m_snumber);
    m_latome2.set_larc_servicename(m_servicename);
}

void LArC::set_latome3(Latome n)
{
    m_latome3 = n;
    m_latome3.set_larc_textnumber(m_snumber);
    m_latome3.set_larc_servicename(m_servicename);
}

void LArC::set_latome4(Latome n)
{
    m_latome4 = n;
    m_latome4.set_larc_textnumber(m_snumber);
    m_latome4.set_larc_servicename(m_servicename);
}
