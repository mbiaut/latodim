#include <iostream>
#include <unistd.h>
#include <stdio.h>
#include <sstream>
#include <map>
#include <vector>
#include <fstream>
#include <string>
#include <bitset>

#include "latodim/ldpb_board.h"


void LdpbBoard::set_number(int n)
{
    m_number = n;
    stringstream ss;
    ss << m_number;
    m_snumber = ss.str();
    while(m_snumber.length() < 3){ m_snumber = "0" + m_snumber; }
}

int LdpbBoard::ping()
{
    string command = "ping -c1 -s1 " + m_servicename + "  > /dev/null 2>&1";
    int x = system(command.c_str());
    int p;
    if (x==0){ p = 1; }
    else{ p = 0; }
    return p;
}

void LdpbBoard::setup_ipbus()
{
    add_in_xml();
    create_ipbus();
}

const uint32_t LdpbBoard::ReadRegisterValue(const string& name)
{
    uint32_t result = 0;
    if (!m_ipbus->read(name, result))
    {
        const string& msg = "Could not read '" + name + "' register.";
        cout << "ERROR : " << msg << endl;
    }
    return result;
}

//ugly method with file juggling
int LdpbBoard::add_in_xml()
{
    //this function add an board at the end of the txt file, and recreate the xml file with the new board
    string begin_xml;
    string const file_connection_name("dim_custom/tmp/LdpbConnections.xml");
    string const file_begin_name("dim_custom/tmp/LdpbConn_begin.txt");

    ofstream file_begin_out(file_begin_name.c_str(), std::ios_base::app);
    if(file_begin_out)    
    {
        file_begin_out << get_xml_text() << endl;
        file_begin_out.close();
    }
    else
    {
        cout << "ERROR: Impossible to open " << file_begin_name << endl;
        return 1;
    }

    ifstream file_begin_in(file_begin_name.c_str());
    if(file_begin_in)
    {
        string line;
        while (getline(file_begin_in, line))
        {
            begin_xml = begin_xml + line + "\n";
        }
        file_begin_in.close();
    }
    else
    {
        cout << "ERROR: Impossible to open " << file_begin_name << endl;
        return 1;
    }

    ofstream file_connection(file_connection_name.c_str());
    if(file_connection)
    {
        file_connection << begin_xml;
        file_connection << "</connections>" << endl;
        file_connection.close();
    }
    else
    {
        cout << "ERROR: Impossible to open " << file_connection_name << endl;
        return 1;
    }
    return 0;
}

string LdpbBoard::get_xml_text()
{
    //string prefix = "EMF_";
    string prefix = "";
    string tab = "            ";
    //string register_path = "/det/lar/project/firmware/LATOME/LATOME_FW-v4.2.0/LATOME/release/fw-48ch-48ch/compilation/latome_hw/release/registers/registers.xml";
    
    string line_xml= "  <connection id=\"" + prefix + get_xmltype() + "-" + m_snumber + "\"" + tab + "uri=\"ipbusudp-2.0://" + m_servicename + ":50001"+ "\"" + tab + "address_table=\"file://" + get_register_path() + "\" />";

    return line_xml;
}

int LdpbBoard::create_ipbus()
{
    m_ipbus = new IpBus("IPbusTest");
    m_ipbus->setLogLevelToDisable();
    //we chose the latome wich we want to talk
    m_ipbus->setBoardId(get_xmltype() + "-" + m_snumber);
    //we open the xml file that represent all accesible board
    if (!m_ipbus->ipBusConfig("file://./dim_custom/tmp/LdpbConnections.xml"))
    {
        cout << "ERROR: Impossible to open ./dim_custom/tmp/LdpbConnections.xml " << endl;
        return 1;
    }
    return 0;
}
