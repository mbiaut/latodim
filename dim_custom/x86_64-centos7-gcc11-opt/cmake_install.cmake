# Install script for directory: /det/lar/usersarea/mbiaut/devarea/latodim/dim_custom

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/installed")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "RelWithDebInfo")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "0")
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

# Set default install directory permissions.
if(NOT DEFINED CMAKE_OBJDUMP)
  set(CMAKE_OBJDUMP "/cvmfs/sft.cern.ch/lcg/releases/binutils/2.36.1-a9696/x86_64-centos7/bin/objdump")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xnoarchx" OR NOT CMAKE_INSTALL_COMPONENT)
  
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xsrcx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/.." TYPE FILE OPTIONAL FILES "/det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/CMakeLists.txt")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xnoarchx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share" TYPE DIRECTORY OPTIONAL FILES "/det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/cmake" FILES_MATCHING REGEX "/[^/]*\\.cmake$" REGEX "/[^/]*\\.in$" REGEX "/\\.svn$" EXCLUDE REGEX "/\\.git$" EXCLUDE)
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xsrcx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/.." TYPE DIRECTORY OPTIONAL FILES "/det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/cmake" FILES_MATCHING REGEX "/[^/]*\\.cmake$" REGEX "/[^/]*\\.in$" REGEX "/\\.svn$" EXCLUDE REGEX "/\\.git$" EXCLUDE)
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xnoarchx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/bin" TYPE PROGRAM OPTIONAL FILES "/det/lar/project/LargOnline/mgmt/lar-cmake/cmake/scripts/run_tdaq")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xdim_custom_noarch-UNKNOWNx" OR NOT CMAKE_INSTALL_COMPONENT)
  
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xdim_custom_x86_64-centos7-gcc11-opt-UNKNOWNx" OR NOT CMAKE_INSTALL_COMPONENT)
  
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xdim_custom_x86_64-centos7-gcc11-opt-UNKNOWNx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/lib/libdim-core.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/lib/libdim-core.so")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/lib/libdim-core.so"
         RPATH "")
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/lib" TYPE SHARED_LIBRARY OPTIONAL FILES "/det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt/lib/libdim-core.so")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/lib/libdim-core.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/lib/libdim-core.so")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/cvmfs/sft.cern.ch/lcg/releases/binutils/2.36.1-a9696/x86_64-centos7/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/lib/libdim-core.so")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xdim_custom_x86_64-centos7-gcc11-opt-UNKNOWNx" OR NOT CMAKE_INSTALL_COMPONENT)
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xdim_custom_x86_64-centos7-gcc11-opt-UNKNOWNx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/lib" TYPE FILE OPTIONAL FILES "/det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt/libdim-core.so.debug")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xdim_custom_x86_64-centos7-gcc11-opt-UNKNOWNx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin/dns" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin/dns")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin/dns"
         RPATH "")
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin" TYPE EXECUTABLE OPTIONAL FILES "/det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt/bin/dns")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin/dns" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin/dns")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin/dns"
         OLD_RPATH "/cvmfs/atlas.cern.ch/repo/sw/tdaq/tdaq/tdaq-09-04-00/installed/external/x86_64-centos7-gcc11-opt/lib:/det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt/lib:"
         NEW_RPATH "")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/cvmfs/sft.cern.ch/lcg/releases/binutils/2.36.1-a9696/x86_64-centos7/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin/dns")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xdim_custom_x86_64-centos7-gcc11-opt-UNKNOWNx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin" TYPE FILE OPTIONAL FILES "/det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt/dns.debug")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xdim_custom_x86_64-centos7-gcc11-opt-UNKNOWNx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin/dim_get_service" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin/dim_get_service")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin/dim_get_service"
         RPATH "")
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin" TYPE EXECUTABLE OPTIONAL FILES "/det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt/bin/dim_get_service")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin/dim_get_service" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin/dim_get_service")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin/dim_get_service"
         OLD_RPATH "/cvmfs/atlas.cern.ch/repo/sw/tdaq/tdaq/tdaq-09-04-00/installed/external/x86_64-centos7-gcc11-opt/lib:/det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt/lib:"
         NEW_RPATH "")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/cvmfs/sft.cern.ch/lcg/releases/binutils/2.36.1-a9696/x86_64-centos7/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin/dim_get_service")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xdim_custom_x86_64-centos7-gcc11-opt-UNKNOWNx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin" TYPE FILE OPTIONAL FILES "/det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt/dim_get_service.debug")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xdim_custom_x86_64-centos7-gcc11-opt-UNKNOWNx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin/dim_send_command" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin/dim_send_command")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin/dim_send_command"
         RPATH "")
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin" TYPE EXECUTABLE OPTIONAL FILES "/det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt/bin/dim_send_command")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin/dim_send_command" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin/dim_send_command")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin/dim_send_command"
         OLD_RPATH "/cvmfs/atlas.cern.ch/repo/sw/tdaq/tdaq/tdaq-09-04-00/installed/external/x86_64-centos7-gcc11-opt/lib:/det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt/lib:"
         NEW_RPATH "")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/cvmfs/sft.cern.ch/lcg/releases/binutils/2.36.1-a9696/x86_64-centos7/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin/dim_send_command")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xdim_custom_x86_64-centos7-gcc11-opt-UNKNOWNx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin" TYPE FILE OPTIONAL FILES "/det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt/dim_send_command.debug")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xdim_custom_x86_64-centos7-gcc11-opt-UNKNOWNx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin/DimBridge" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin/DimBridge")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin/DimBridge"
         RPATH "")
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin" TYPE EXECUTABLE OPTIONAL FILES "/det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt/bin/DimBridge")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin/DimBridge" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin/DimBridge")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin/DimBridge"
         OLD_RPATH "/cvmfs/atlas.cern.ch/repo/sw/tdaq/tdaq/tdaq-09-04-00/installed/external/x86_64-centos7-gcc11-opt/lib:/det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt/lib:"
         NEW_RPATH "")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/cvmfs/sft.cern.ch/lcg/releases/binutils/2.36.1-a9696/x86_64-centos7/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin/DimBridge")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xdim_custom_x86_64-centos7-gcc11-opt-UNKNOWNx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin" TYPE FILE OPTIONAL FILES "/det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt/DimBridge.debug")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xdim_custom_x86_64-centos7-gcc11-opt-UNKNOWNx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin/checkDns" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin/checkDns")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin/checkDns"
         RPATH "")
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin" TYPE EXECUTABLE OPTIONAL FILES "/det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt/bin/checkDns")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin/checkDns" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin/checkDns")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin/checkDns"
         OLD_RPATH "/cvmfs/atlas.cern.ch/repo/sw/tdaq/tdaq/tdaq-09-04-00/installed/external/x86_64-centos7-gcc11-opt/lib:/det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt/lib:"
         NEW_RPATH "")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/cvmfs/sft.cern.ch/lcg/releases/binutils/2.36.1-a9696/x86_64-centos7/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin/checkDns")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xdim_custom_x86_64-centos7-gcc11-opt-UNKNOWNx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin" TYPE FILE OPTIONAL FILES "/det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt/checkDns.debug")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xdim_custom_x86_64-centos7-gcc11-opt-UNKNOWNx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin/checkDimServers" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin/checkDimServers")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin/checkDimServers"
         RPATH "")
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin" TYPE EXECUTABLE OPTIONAL FILES "/det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt/bin/checkDimServers")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin/checkDimServers" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin/checkDimServers")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin/checkDimServers"
         OLD_RPATH "/cvmfs/atlas.cern.ch/repo/sw/tdaq/tdaq/tdaq-09-04-00/installed/external/x86_64-centos7-gcc11-opt/lib:/det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt/lib:"
         NEW_RPATH "")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/cvmfs/sft.cern.ch/lcg/releases/binutils/2.36.1-a9696/x86_64-centos7/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin/checkDimServers")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xdim_custom_x86_64-centos7-gcc11-opt-UNKNOWNx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin" TYPE FILE OPTIONAL FILES "/det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt/checkDimServers.debug")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xdim_custom_x86_64-centos7-gcc11-opt-UNKNOWNx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin/did" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin/did")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin/did"
         RPATH "")
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin" TYPE EXECUTABLE OPTIONAL FILES "/det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt/bin/did")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin/did" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin/did")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin/did"
         OLD_RPATH "/cvmfs/atlas.cern.ch/repo/sw/tdaq/tdaq/tdaq-09-04-00/installed/external/x86_64-centos7-gcc11-opt/lib:/det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt/lib:"
         NEW_RPATH "")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/cvmfs/sft.cern.ch/lcg/releases/binutils/2.36.1-a9696/x86_64-centos7/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin/did")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xdim_custom_x86_64-centos7-gcc11-opt-UNKNOWNx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin" TYPE FILE OPTIONAL FILES "/det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt/did.debug")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xdim_custom_x86_64-centos7-gcc11-opt-UNKNOWNx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin/webDid" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin/webDid")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin/webDid"
         RPATH "")
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin" TYPE EXECUTABLE OPTIONAL FILES "/det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt/bin/webDid")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin/webDid" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin/webDid")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin/webDid"
         OLD_RPATH "/cvmfs/atlas.cern.ch/repo/sw/tdaq/tdaq/tdaq-09-04-00/installed/external/x86_64-centos7-gcc11-opt/lib:/det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt/lib:"
         NEW_RPATH "")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/cvmfs/sft.cern.ch/lcg/releases/binutils/2.36.1-a9696/x86_64-centos7/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin/webDid")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xdim_custom_x86_64-centos7-gcc11-opt-UNKNOWNx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin" TYPE FILE OPTIONAL FILES "/det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt/webDid.debug")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xdim_custom_x86_64-centos7-gcc11-opt-UNKNOWNx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin/test_server" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin/test_server")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin/test_server"
         RPATH "")
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin" TYPE EXECUTABLE OPTIONAL FILES "/det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt/bin/test_server")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin/test_server" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin/test_server")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin/test_server"
         OLD_RPATH "/cvmfs/atlas.cern.ch/repo/sw/tdaq/tdaq/tdaq-09-04-00/installed/external/x86_64-centos7-gcc11-opt/lib:/det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt/lib:"
         NEW_RPATH "")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/cvmfs/sft.cern.ch/lcg/releases/binutils/2.36.1-a9696/x86_64-centos7/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin/test_server")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xdim_custom_x86_64-centos7-gcc11-opt-UNKNOWNx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin" TYPE FILE OPTIONAL FILES "/det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt/test_server.debug")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xdim_custom_x86_64-centos7-gcc11-opt-UNKNOWNx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin/test_client" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin/test_client")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin/test_client"
         RPATH "")
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin" TYPE EXECUTABLE OPTIONAL FILES "/det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt/bin/test_client")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin/test_client" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin/test_client")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin/test_client"
         OLD_RPATH "/cvmfs/atlas.cern.ch/repo/sw/tdaq/tdaq/tdaq-09-04-00/installed/external/x86_64-centos7-gcc11-opt/lib:/det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt/lib:"
         NEW_RPATH "")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/cvmfs/sft.cern.ch/lcg/releases/binutils/2.36.1-a9696/x86_64-centos7/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin/test_client")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xdim_custom_x86_64-centos7-gcc11-opt-UNKNOWNx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin" TYPE FILE OPTIONAL FILES "/det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt/test_client.debug")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xdim_custom_x86_64-centos7-gcc11-opt-UNKNOWNx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin/testServer" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin/testServer")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin/testServer"
         RPATH "")
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin" TYPE EXECUTABLE OPTIONAL FILES "/det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt/bin/testServer")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin/testServer" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin/testServer")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin/testServer"
         OLD_RPATH "/cvmfs/atlas.cern.ch/repo/sw/tdaq/tdaq/tdaq-09-04-00/installed/external/x86_64-centos7-gcc11-opt/lib:/det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt/lib:"
         NEW_RPATH "")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/cvmfs/sft.cern.ch/lcg/releases/binutils/2.36.1-a9696/x86_64-centos7/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin/testServer")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xdim_custom_x86_64-centos7-gcc11-opt-UNKNOWNx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin" TYPE FILE OPTIONAL FILES "/det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt/testServer.debug")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xdim_custom_x86_64-centos7-gcc11-opt-UNKNOWNx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin/testClient" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin/testClient")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin/testClient"
         RPATH "")
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin" TYPE EXECUTABLE OPTIONAL FILES "/det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt/bin/testClient")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin/testClient" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin/testClient")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin/testClient"
         OLD_RPATH "/cvmfs/atlas.cern.ch/repo/sw/tdaq/tdaq/tdaq-09-04-00/installed/external/x86_64-centos7-gcc11-opt/lib:/det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt/lib:"
         NEW_RPATH "")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/cvmfs/sft.cern.ch/lcg/releases/binutils/2.36.1-a9696/x86_64-centos7/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin/testClient")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xdim_custom_x86_64-centos7-gcc11-opt-UNKNOWNx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin" TYPE FILE OPTIONAL FILES "/det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt/testClient.debug")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xdim_custom_x86_64-centos7-gcc11-opt-UNKNOWNx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin/benchServer" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin/benchServer")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin/benchServer"
         RPATH "")
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin" TYPE EXECUTABLE OPTIONAL FILES "/det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt/bin/benchServer")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin/benchServer" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin/benchServer")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin/benchServer"
         OLD_RPATH "/cvmfs/atlas.cern.ch/repo/sw/tdaq/tdaq/tdaq-09-04-00/installed/external/x86_64-centos7-gcc11-opt/lib:/det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt/lib:"
         NEW_RPATH "")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/cvmfs/sft.cern.ch/lcg/releases/binutils/2.36.1-a9696/x86_64-centos7/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin/benchServer")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xdim_custom_x86_64-centos7-gcc11-opt-UNKNOWNx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin" TYPE FILE OPTIONAL FILES "/det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt/benchServer.debug")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xdim_custom_x86_64-centos7-gcc11-opt-UNKNOWNx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin/benchClient" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin/benchClient")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin/benchClient"
         RPATH "")
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin" TYPE EXECUTABLE OPTIONAL FILES "/det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt/bin/benchClient")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin/benchClient" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin/benchClient")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin/benchClient"
         OLD_RPATH "/cvmfs/atlas.cern.ch/repo/sw/tdaq/tdaq/tdaq-09-04-00/installed/external/x86_64-centos7-gcc11-opt/lib:/det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt/lib:"
         NEW_RPATH "")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/cvmfs/sft.cern.ch/lcg/releases/binutils/2.36.1-a9696/x86_64-centos7/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin/benchClient")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xdim_custom_x86_64-centos7-gcc11-opt-UNKNOWNx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin" TYPE FILE OPTIONAL FILES "/det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt/benchClient.debug")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xdim_custom_x86_64-centos7-gcc11-opt-UNKNOWNx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin/latodim" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin/latodim")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin/latodim"
         RPATH "")
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin" TYPE EXECUTABLE OPTIONAL FILES "/det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt/bin/latodim")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin/latodim" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin/latodim")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin/latodim"
         OLD_RPATH "/cvmfs/atlas.cern.ch/repo/sw/tdaq/tdaq/tdaq-09-04-00/installed/external/x86_64-centos7-gcc11-opt/lib:/det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt/lib:/det/lar/project/LargOnline/releases/LargOnline-940-47-00/IpBus/v1r7/x86_64-centos7-gcc11-opt/lib:/det/lar/project/LargOnline/releases/LargOnline-940-47-00/Base/v16r4/x86_64-centos7-gcc11-opt/lib:"
         NEW_RPATH "")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/cvmfs/sft.cern.ch/lcg/releases/binutils/2.36.1-a9696/x86_64-centos7/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin/latodim")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xdim_custom_x86_64-centos7-gcc11-opt-UNKNOWNx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/x86_64-centos7-gcc11-opt/bin" TYPE FILE OPTIONAL FILES "/det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt/latodim.debug")
endif()

if(CMAKE_INSTALL_COMPONENT)
  set(CMAKE_INSTALL_MANIFEST "install_manifest_${CMAKE_INSTALL_COMPONENT}.txt")
else()
  set(CMAKE_INSTALL_MANIFEST "install_manifest.txt")
endif()

string(REPLACE ";" "\n" CMAKE_INSTALL_MANIFEST_CONTENT
       "${CMAKE_INSTALL_MANIFEST_FILES}")
file(WRITE "/det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt/${CMAKE_INSTALL_MANIFEST}"
     "${CMAKE_INSTALL_MANIFEST_CONTENT}")
