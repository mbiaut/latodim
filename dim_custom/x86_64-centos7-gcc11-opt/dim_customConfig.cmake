# Config file for the dim_custom package


####### Expanded from @PACKAGE_INIT@ by configure_package_config_file() #######
####### Any changes to this file will be overwritten by the next CMake run ####
####### The input file was dim_customConfig.cmake.in                            ########

get_filename_component(PACKAGE_PREFIX_DIR "${CMAKE_CURRENT_LIST_DIR}/../../../../" ABSOLUTE)

macro(set_and_check _var _file)
  set(${_var} "${_file}")
  if(NOT EXISTS "${_file}")
    message(FATAL_ERROR "File or directory ${_file} referenced by variable ${_var} does not exist !")
  endif()
endmacro()

macro(check_required_components _NAME)
  foreach(comp ${${_NAME}_FIND_COMPONENTS})
    if(NOT ${_NAME}_${comp}_FOUND)
      if(${_NAME}_FIND_REQUIRED_${comp})
        set(${_NAME}_FOUND FALSE)
      endif()
    endif()
  endforeach()
endmacro()

####################################################################################

# library dependencies (contains definitions for IMPORTED targets)
set(_pos1)
set(_pos2)

string(FIND ${CMAKE_CURRENT_LIST_DIR} ${LAR_CURRENT_PROJECT_ROOT} _pos1)
string(FIND ${LAR_CURRENT_PROJECT_ROOT} ${PROJECT_SOURCE_DIR} _pos2)

if(${_pos1} EQUAL -1)
  if(EXISTS "${CMAKE_CURRENT_LIST_DIR}/dim_customTargets.cmake")
    include("${CMAKE_CURRENT_LIST_DIR}/dim_customTargets.cmake")
  endif()
elseif(${_pos2} EQUAL -1)
  if(EXISTS "${CMAKE_CURRENT_LIST_DIR}/dim_customTargets.cmake")
    include("${CMAKE_CURRENT_LIST_DIR}/dim_customTargets.cmake")
  endif()
endif()

unset(_pos1)
unset(_pos2)

if(EXISTS "${CMAKE_CURRENT_LIST_DIR}/dim_customJavaTargets.cmake")
  include("${CMAKE_CURRENT_LIST_DIR}/dim_customJavaTargets.cmake")
endif()

include(LArUsePackage)

lar_use_package(Base v16r*)
lar_use_package(IpBus v1r*)
