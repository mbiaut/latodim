# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.19

# Delete rule output on recipe failure.
.DELETE_ON_ERROR:


#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:


# Disable VCS-based implicit rules.
% : %,v


# Disable VCS-based implicit rules.
% : RCS/%


# Disable VCS-based implicit rules.
% : RCS/%,v


# Disable VCS-based implicit rules.
% : SCCS/s.%


# Disable VCS-based implicit rules.
% : s.%


.SUFFIXES: .hpux_make_needs_suffix_list


# Command-line flag to silence nested $(MAKE).
$(VERBOSE)MAKESILENT = -s

#Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:

.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /cvmfs/atlas.cern.ch/repo/sw/tdaq/tools/x86_64-centos7/CMake/3.19.6/bin/cmake

# The command to remove a file.
RM = /cvmfs/atlas.cern.ch/repo/sw/tdaq/tools/x86_64-centos7/CMake/3.19.6/bin/cmake -E rm -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /det/lar/usersarea/mbiaut/devarea/latodim/dim_custom

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt

# Include any dependencies generated for this target.
include CMakeFiles/test_server.dir/depend.make

# Include the progress variables for this target.
include CMakeFiles/test_server.dir/progress.make

# Include the compile flags for this target's objects.
include CMakeFiles/test_server.dir/flags.make

CMakeFiles/test_server.dir/src/examples/test_server.c.o: CMakeFiles/test_server.dir/flags.make
CMakeFiles/test_server.dir/src/examples/test_server.c.o: ../src/examples/test_server.c
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=/det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt/CMakeFiles --progress-num=$(CMAKE_PROGRESS_1) "Building C object CMakeFiles/test_server.dir/src/examples/test_server.c.o"
	/cvmfs/sft.cern.ch/lcg/releases/gcc/11.1.0-e80bf/x86_64-centos7/bin/gcc $(C_DEFINES) $(C_INCLUDES) $(C_FLAGS) -o CMakeFiles/test_server.dir/src/examples/test_server.c.o -c /det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/src/examples/test_server.c

CMakeFiles/test_server.dir/src/examples/test_server.c.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing C source to CMakeFiles/test_server.dir/src/examples/test_server.c.i"
	/cvmfs/sft.cern.ch/lcg/releases/gcc/11.1.0-e80bf/x86_64-centos7/bin/gcc $(C_DEFINES) $(C_INCLUDES) $(C_FLAGS) -E /det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/src/examples/test_server.c > CMakeFiles/test_server.dir/src/examples/test_server.c.i

CMakeFiles/test_server.dir/src/examples/test_server.c.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling C source to assembly CMakeFiles/test_server.dir/src/examples/test_server.c.s"
	/cvmfs/sft.cern.ch/lcg/releases/gcc/11.1.0-e80bf/x86_64-centos7/bin/gcc $(C_DEFINES) $(C_INCLUDES) $(C_FLAGS) -S /det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/src/examples/test_server.c -o CMakeFiles/test_server.dir/src/examples/test_server.c.s

# Object files for target test_server
test_server_OBJECTS = \
"CMakeFiles/test_server.dir/src/examples/test_server.c.o"

# External object files for target test_server
test_server_EXTERNAL_OBJECTS =

bin/test_server: CMakeFiles/test_server.dir/src/examples/test_server.c.o
bin/test_server: CMakeFiles/test_server.dir/build.make
bin/test_server: lib/libdim-core.so
bin/test_server: CMakeFiles/test_server.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --bold --progress-dir=/det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt/CMakeFiles --progress-num=$(CMAKE_PROGRESS_2) "Linking C executable bin/test_server"
	$(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/test_server.dir/link.txt --verbose=$(VERBOSE)
	/cvmfs/sft.cern.ch/lcg/releases/binutils/2.36.1-a9696/x86_64-centos7/bin/objcopy --only-keep-debug /det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt/bin/test_server /det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt/bin/test_server.debug
	/cvmfs/sft.cern.ch/lcg/releases/binutils/2.36.1-a9696/x86_64-centos7/bin/objcopy --strip-debug /det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt/bin/test_server
	/cvmfs/sft.cern.ch/lcg/releases/binutils/2.36.1-a9696/x86_64-centos7/bin/objcopy --add-gnu-debuglink=bin/test_server.debug /det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt/bin/test_server

# Rule to build all files generated by this target.
CMakeFiles/test_server.dir/build: bin/test_server

.PHONY : CMakeFiles/test_server.dir/build

CMakeFiles/test_server.dir/clean:
	$(CMAKE_COMMAND) -P CMakeFiles/test_server.dir/cmake_clean.cmake
.PHONY : CMakeFiles/test_server.dir/clean

CMakeFiles/test_server.dir/depend:
	cd /det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /det/lar/usersarea/mbiaut/devarea/latodim/dim_custom /det/lar/usersarea/mbiaut/devarea/latodim/dim_custom /det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt /det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt /det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt/CMakeFiles/test_server.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : CMakeFiles/test_server.dir/depend

