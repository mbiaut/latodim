# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.19

# Delete rule output on recipe failure.
.DELETE_ON_ERROR:


#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:


# Disable VCS-based implicit rules.
% : %,v


# Disable VCS-based implicit rules.
% : RCS/%


# Disable VCS-based implicit rules.
% : RCS/%,v


# Disable VCS-based implicit rules.
% : SCCS/s.%


# Disable VCS-based implicit rules.
% : s.%


.SUFFIXES: .hpux_make_needs_suffix_list


# Command-line flag to silence nested $(MAKE).
$(VERBOSE)MAKESILENT = -s

#Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:

.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /cvmfs/atlas.cern.ch/repo/sw/tdaq/tools/x86_64-centos7/CMake/3.19.6/bin/cmake

# The command to remove a file.
RM = /cvmfs/atlas.cern.ch/repo/sw/tdaq/tools/x86_64-centos7/CMake/3.19.6/bin/cmake -E rm -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /det/lar/usersarea/mbiaut/devarea/latodim/dim_custom

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt

# Include any dependencies generated for this target.
include CMakeFiles/dim_send_command.dir/depend.make

# Include the progress variables for this target.
include CMakeFiles/dim_send_command.dir/progress.make

# Include the compile flags for this target's objects.
include CMakeFiles/dim_send_command.dir/flags.make

CMakeFiles/dim_send_command.dir/src/util/dim_send_command.c.o: CMakeFiles/dim_send_command.dir/flags.make
CMakeFiles/dim_send_command.dir/src/util/dim_send_command.c.o: ../src/util/dim_send_command.c
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=/det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt/CMakeFiles --progress-num=$(CMAKE_PROGRESS_1) "Building C object CMakeFiles/dim_send_command.dir/src/util/dim_send_command.c.o"
	/cvmfs/sft.cern.ch/lcg/releases/gcc/11.1.0-e80bf/x86_64-centos7/bin/gcc $(C_DEFINES) $(C_INCLUDES) $(C_FLAGS) -o CMakeFiles/dim_send_command.dir/src/util/dim_send_command.c.o -c /det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/src/util/dim_send_command.c

CMakeFiles/dim_send_command.dir/src/util/dim_send_command.c.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing C source to CMakeFiles/dim_send_command.dir/src/util/dim_send_command.c.i"
	/cvmfs/sft.cern.ch/lcg/releases/gcc/11.1.0-e80bf/x86_64-centos7/bin/gcc $(C_DEFINES) $(C_INCLUDES) $(C_FLAGS) -E /det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/src/util/dim_send_command.c > CMakeFiles/dim_send_command.dir/src/util/dim_send_command.c.i

CMakeFiles/dim_send_command.dir/src/util/dim_send_command.c.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling C source to assembly CMakeFiles/dim_send_command.dir/src/util/dim_send_command.c.s"
	/cvmfs/sft.cern.ch/lcg/releases/gcc/11.1.0-e80bf/x86_64-centos7/bin/gcc $(C_DEFINES) $(C_INCLUDES) $(C_FLAGS) -S /det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/src/util/dim_send_command.c -o CMakeFiles/dim_send_command.dir/src/util/dim_send_command.c.s

# Object files for target dim_send_command
dim_send_command_OBJECTS = \
"CMakeFiles/dim_send_command.dir/src/util/dim_send_command.c.o"

# External object files for target dim_send_command
dim_send_command_EXTERNAL_OBJECTS =

bin/dim_send_command: CMakeFiles/dim_send_command.dir/src/util/dim_send_command.c.o
bin/dim_send_command: CMakeFiles/dim_send_command.dir/build.make
bin/dim_send_command: lib/libdim-core.so
bin/dim_send_command: CMakeFiles/dim_send_command.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --bold --progress-dir=/det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt/CMakeFiles --progress-num=$(CMAKE_PROGRESS_2) "Linking C executable bin/dim_send_command"
	$(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/dim_send_command.dir/link.txt --verbose=$(VERBOSE)
	/cvmfs/sft.cern.ch/lcg/releases/binutils/2.36.1-a9696/x86_64-centos7/bin/objcopy --only-keep-debug /det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt/bin/dim_send_command /det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt/bin/dim_send_command.debug
	/cvmfs/sft.cern.ch/lcg/releases/binutils/2.36.1-a9696/x86_64-centos7/bin/objcopy --strip-debug /det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt/bin/dim_send_command
	/cvmfs/sft.cern.ch/lcg/releases/binutils/2.36.1-a9696/x86_64-centos7/bin/objcopy --add-gnu-debuglink=bin/dim_send_command.debug /det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt/bin/dim_send_command

# Rule to build all files generated by this target.
CMakeFiles/dim_send_command.dir/build: bin/dim_send_command

.PHONY : CMakeFiles/dim_send_command.dir/build

CMakeFiles/dim_send_command.dir/clean:
	$(CMAKE_COMMAND) -P CMakeFiles/dim_send_command.dir/cmake_clean.cmake
.PHONY : CMakeFiles/dim_send_command.dir/clean

CMakeFiles/dim_send_command.dir/depend:
	cd /det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /det/lar/usersarea/mbiaut/devarea/latodim/dim_custom /det/lar/usersarea/mbiaut/devarea/latodim/dim_custom /det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt /det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt /det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt/CMakeFiles/dim_send_command.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : CMakeFiles/dim_send_command.dir/depend

