file(REMOVE_RECURSE
  "DimBridge.debug"
  "benchClient.debug"
  "benchServer.debug"
  "checkDimServers.debug"
  "checkDns.debug"
  "did.debug"
  "dim_get_service.debug"
  "dim_send_command.debug"
  "dns.debug"
  "latodim.debug"
  "libdim-core.so.debug"
  "testClient.debug"
  "testServer.debug"
  "test_client.debug"
  "test_server.debug"
  "webDid.debug"
  "CMakeFiles/dim_send_command.dir/src/util/dim_send_command.c.o"
  "bin/dim_send_command"
  "bin/dim_send_command.pdb"
)

# Per-language clean rules from dependency scanning.
foreach(lang C)
  include(CMakeFiles/dim_send_command.dir/cmake_clean_${lang}.cmake OPTIONAL)
endforeach()
