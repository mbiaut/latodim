file(REMOVE_RECURSE
  "DimBridge.debug"
  "benchClient.debug"
  "benchServer.debug"
  "checkDimServers.debug"
  "checkDns.debug"
  "did.debug"
  "dim_get_service.debug"
  "dim_send_command.debug"
  "dns.debug"
  "latodim.debug"
  "libdim-core.so.debug"
  "testClient.debug"
  "testServer.debug"
  "test_client.debug"
  "test_server.debug"
  "webDid.debug"
  "CMakeFiles/webDid.dir/src/webDid/webDid.c.o"
  "CMakeFiles/webDid.dir/src/webDid/webServer.c.o"
  "CMakeFiles/webDid.dir/src/webDid/webSmiD.c.o"
  "CMakeFiles/webDid.dir/src/webDid/webTcpip.c.o"
  "bin/webDid"
  "bin/webDid.pdb"
)

# Per-language clean rules from dependency scanning.
foreach(lang C)
  include(CMakeFiles/webDid.dir/cmake_clean_${lang}.cmake OPTIONAL)
endforeach()
