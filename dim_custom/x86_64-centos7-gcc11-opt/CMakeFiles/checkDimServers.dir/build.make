# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.19

# Delete rule output on recipe failure.
.DELETE_ON_ERROR:


#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:


# Disable VCS-based implicit rules.
% : %,v


# Disable VCS-based implicit rules.
% : RCS/%


# Disable VCS-based implicit rules.
% : RCS/%,v


# Disable VCS-based implicit rules.
% : SCCS/s.%


# Disable VCS-based implicit rules.
% : s.%


.SUFFIXES: .hpux_make_needs_suffix_list


# Command-line flag to silence nested $(MAKE).
$(VERBOSE)MAKESILENT = -s

#Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:

.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /cvmfs/atlas.cern.ch/repo/sw/tdaq/tools/x86_64-centos7/CMake/3.19.6/bin/cmake

# The command to remove a file.
RM = /cvmfs/atlas.cern.ch/repo/sw/tdaq/tools/x86_64-centos7/CMake/3.19.6/bin/cmake -E rm -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /det/lar/usersarea/mbiaut/devarea/latodim/dim_custom

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt

# Include any dependencies generated for this target.
include CMakeFiles/checkDimServers.dir/depend.make

# Include the progress variables for this target.
include CMakeFiles/checkDimServers.dir/progress.make

# Include the compile flags for this target's objects.
include CMakeFiles/checkDimServers.dir/flags.make

CMakeFiles/checkDimServers.dir/src/util/check_dim_servers.cxx.o: CMakeFiles/checkDimServers.dir/flags.make
CMakeFiles/checkDimServers.dir/src/util/check_dim_servers.cxx.o: ../src/util/check_dim_servers.cxx
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=/det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt/CMakeFiles --progress-num=$(CMAKE_PROGRESS_1) "Building CXX object CMakeFiles/checkDimServers.dir/src/util/check_dim_servers.cxx.o"
	/cvmfs/sft.cern.ch/lcg/releases/gcc/11.1.0-e80bf/x86_64-centos7/bin/g++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -o CMakeFiles/checkDimServers.dir/src/util/check_dim_servers.cxx.o -c /det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/src/util/check_dim_servers.cxx

CMakeFiles/checkDimServers.dir/src/util/check_dim_servers.cxx.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/checkDimServers.dir/src/util/check_dim_servers.cxx.i"
	/cvmfs/sft.cern.ch/lcg/releases/gcc/11.1.0-e80bf/x86_64-centos7/bin/g++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -E /det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/src/util/check_dim_servers.cxx > CMakeFiles/checkDimServers.dir/src/util/check_dim_servers.cxx.i

CMakeFiles/checkDimServers.dir/src/util/check_dim_servers.cxx.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/checkDimServers.dir/src/util/check_dim_servers.cxx.s"
	/cvmfs/sft.cern.ch/lcg/releases/gcc/11.1.0-e80bf/x86_64-centos7/bin/g++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -S /det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/src/util/check_dim_servers.cxx -o CMakeFiles/checkDimServers.dir/src/util/check_dim_servers.cxx.s

# Object files for target checkDimServers
checkDimServers_OBJECTS = \
"CMakeFiles/checkDimServers.dir/src/util/check_dim_servers.cxx.o"

# External object files for target checkDimServers
checkDimServers_EXTERNAL_OBJECTS =

bin/checkDimServers: CMakeFiles/checkDimServers.dir/src/util/check_dim_servers.cxx.o
bin/checkDimServers: CMakeFiles/checkDimServers.dir/build.make
bin/checkDimServers: lib/libdim-core.so
bin/checkDimServers: CMakeFiles/checkDimServers.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --bold --progress-dir=/det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt/CMakeFiles --progress-num=$(CMAKE_PROGRESS_2) "Linking CXX executable bin/checkDimServers"
	$(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/checkDimServers.dir/link.txt --verbose=$(VERBOSE)
	/cvmfs/sft.cern.ch/lcg/releases/binutils/2.36.1-a9696/x86_64-centos7/bin/objcopy --only-keep-debug /det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt/bin/checkDimServers /det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt/bin/checkDimServers.debug
	/cvmfs/sft.cern.ch/lcg/releases/binutils/2.36.1-a9696/x86_64-centos7/bin/objcopy --strip-debug /det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt/bin/checkDimServers
	/cvmfs/sft.cern.ch/lcg/releases/binutils/2.36.1-a9696/x86_64-centos7/bin/objcopy --add-gnu-debuglink=bin/checkDimServers.debug /det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt/bin/checkDimServers

# Rule to build all files generated by this target.
CMakeFiles/checkDimServers.dir/build: bin/checkDimServers

.PHONY : CMakeFiles/checkDimServers.dir/build

CMakeFiles/checkDimServers.dir/clean:
	$(CMAKE_COMMAND) -P CMakeFiles/checkDimServers.dir/cmake_clean.cmake
.PHONY : CMakeFiles/checkDimServers.dir/clean

CMakeFiles/checkDimServers.dir/depend:
	cd /det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /det/lar/usersarea/mbiaut/devarea/latodim/dim_custom /det/lar/usersarea/mbiaut/devarea/latodim/dim_custom /det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt /det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt /det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt/CMakeFiles/checkDimServers.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : CMakeFiles/checkDimServers.dir/depend

