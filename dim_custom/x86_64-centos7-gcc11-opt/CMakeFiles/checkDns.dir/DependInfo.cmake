# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/src/util/check_dns.cxx" "/det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt/CMakeFiles/checkDns.dir/src/util/check_dns.cxx.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "MIPSEL"
  "PROTOCOL=1"
  "TDAQ_PACKAGE_NAME=\"dim_custom\""
  "_GNU_SOURCE"
  "_REENTRANT"
  "__USE_XOPEN2K8"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "."
  "../"
  "/cvmfs/atlas.cern.ch/repo/sw/tdaq/tdaq-common/tdaq-common-04-04-00/installed/include"
  "/cvmfs/atlas.cern.ch/repo/sw/tdaq/tdaq-common/tdaq-common-04-04-00/installed/x86_64-centos7-gcc11-opt/include"
  "/cvmfs/atlas.cern.ch/repo/sw/tdaq/tdaq-common/tdaq-common-04-04-00/installed/external/x86_64-centos7-gcc11-opt/include"
  "/cvmfs/atlas.cern.ch/repo/sw/tdaq/tdaq/tdaq-09-04-00/installed/include"
  "/cvmfs/atlas.cern.ch/repo/sw/tdaq/tdaq/tdaq-09-04-00/installed/x86_64-centos7-gcc11-opt/include"
  "/cvmfs/atlas.cern.ch/repo/sw/tdaq/tdaq/tdaq-09-04-00/installed/external/x86_64-centos7-gcc11-opt/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt/CMakeFiles/dim-core.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
