file(REMOVE_RECURSE
  "DimBridge.debug"
  "benchClient.debug"
  "benchServer.debug"
  "checkDimServers.debug"
  "checkDns.debug"
  "did.debug"
  "dim_get_service.debug"
  "dim_send_command.debug"
  "dns.debug"
  "latodim.debug"
  "libdim-core.so.debug"
  "testClient.debug"
  "testServer.debug"
  "test_client.debug"
  "test_server.debug"
  "webDid.debug"
  "CMakeFiles/dim_get_service.dir/src/util/dim_get_service.c.o"
  "bin/dim_get_service"
  "bin/dim_get_service.pdb"
)

# Per-language clean rules from dependency scanning.
foreach(lang C)
  include(CMakeFiles/dim_get_service.dir/cmake_clean_${lang}.cmake OPTIONAL)
endforeach()
