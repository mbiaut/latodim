file(REMOVE_RECURSE
  "DimBridge.debug"
  "benchClient.debug"
  "benchServer.debug"
  "checkDimServers.debug"
  "checkDns.debug"
  "did.debug"
  "dim_get_service.debug"
  "dim_send_command.debug"
  "dns.debug"
  "latodim.debug"
  "libdim-core.so.debug"
  "testClient.debug"
  "testServer.debug"
  "test_client.debug"
  "test_server.debug"
  "webDid.debug"
  "CMakeFiles/benchServer.dir/src/benchmark/benchServer.cxx.o"
  "bin/benchServer"
  "bin/benchServer.pdb"
)

# Per-language clean rules from dependency scanning.
foreach(lang CXX)
  include(CMakeFiles/benchServer.dir/cmake_clean_${lang}.cmake OPTIONAL)
endforeach()
