# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.19

# Delete rule output on recipe failure.
.DELETE_ON_ERROR:


#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:


# Disable VCS-based implicit rules.
% : %,v


# Disable VCS-based implicit rules.
% : RCS/%


# Disable VCS-based implicit rules.
% : RCS/%,v


# Disable VCS-based implicit rules.
% : SCCS/s.%


# Disable VCS-based implicit rules.
% : s.%


.SUFFIXES: .hpux_make_needs_suffix_list


# Command-line flag to silence nested $(MAKE).
$(VERBOSE)MAKESILENT = -s

#Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:

.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /cvmfs/atlas.cern.ch/repo/sw/tdaq/tools/x86_64-centos7/CMake/3.19.6/bin/cmake

# The command to remove a file.
RM = /cvmfs/atlas.cern.ch/repo/sw/tdaq/tools/x86_64-centos7/CMake/3.19.6/bin/cmake -E rm -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /det/lar/usersarea/mbiaut/devarea/latodim/dim_custom

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt

# Include any dependencies generated for this target.
include CMakeFiles/benchClient.dir/depend.make

# Include the progress variables for this target.
include CMakeFiles/benchClient.dir/progress.make

# Include the compile flags for this target's objects.
include CMakeFiles/benchClient.dir/flags.make

CMakeFiles/benchClient.dir/src/benchmark/benchClient.cxx.o: CMakeFiles/benchClient.dir/flags.make
CMakeFiles/benchClient.dir/src/benchmark/benchClient.cxx.o: ../src/benchmark/benchClient.cxx
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=/det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt/CMakeFiles --progress-num=$(CMAKE_PROGRESS_1) "Building CXX object CMakeFiles/benchClient.dir/src/benchmark/benchClient.cxx.o"
	/cvmfs/sft.cern.ch/lcg/releases/gcc/11.1.0-e80bf/x86_64-centos7/bin/g++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -o CMakeFiles/benchClient.dir/src/benchmark/benchClient.cxx.o -c /det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/src/benchmark/benchClient.cxx

CMakeFiles/benchClient.dir/src/benchmark/benchClient.cxx.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/benchClient.dir/src/benchmark/benchClient.cxx.i"
	/cvmfs/sft.cern.ch/lcg/releases/gcc/11.1.0-e80bf/x86_64-centos7/bin/g++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -E /det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/src/benchmark/benchClient.cxx > CMakeFiles/benchClient.dir/src/benchmark/benchClient.cxx.i

CMakeFiles/benchClient.dir/src/benchmark/benchClient.cxx.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/benchClient.dir/src/benchmark/benchClient.cxx.s"
	/cvmfs/sft.cern.ch/lcg/releases/gcc/11.1.0-e80bf/x86_64-centos7/bin/g++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -S /det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/src/benchmark/benchClient.cxx -o CMakeFiles/benchClient.dir/src/benchmark/benchClient.cxx.s

# Object files for target benchClient
benchClient_OBJECTS = \
"CMakeFiles/benchClient.dir/src/benchmark/benchClient.cxx.o"

# External object files for target benchClient
benchClient_EXTERNAL_OBJECTS =

bin/benchClient: CMakeFiles/benchClient.dir/src/benchmark/benchClient.cxx.o
bin/benchClient: CMakeFiles/benchClient.dir/build.make
bin/benchClient: lib/libdim-core.so
bin/benchClient: CMakeFiles/benchClient.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --bold --progress-dir=/det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt/CMakeFiles --progress-num=$(CMAKE_PROGRESS_2) "Linking CXX executable bin/benchClient"
	$(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/benchClient.dir/link.txt --verbose=$(VERBOSE)
	/cvmfs/sft.cern.ch/lcg/releases/binutils/2.36.1-a9696/x86_64-centos7/bin/objcopy --only-keep-debug /det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt/bin/benchClient /det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt/bin/benchClient.debug
	/cvmfs/sft.cern.ch/lcg/releases/binutils/2.36.1-a9696/x86_64-centos7/bin/objcopy --strip-debug /det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt/bin/benchClient
	/cvmfs/sft.cern.ch/lcg/releases/binutils/2.36.1-a9696/x86_64-centos7/bin/objcopy --add-gnu-debuglink=bin/benchClient.debug /det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt/bin/benchClient

# Rule to build all files generated by this target.
CMakeFiles/benchClient.dir/build: bin/benchClient

.PHONY : CMakeFiles/benchClient.dir/build

CMakeFiles/benchClient.dir/clean:
	$(CMAKE_COMMAND) -P CMakeFiles/benchClient.dir/cmake_clean.cmake
.PHONY : CMakeFiles/benchClient.dir/clean

CMakeFiles/benchClient.dir/depend:
	cd /det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /det/lar/usersarea/mbiaut/devarea/latodim/dim_custom /det/lar/usersarea/mbiaut/devarea/latodim/dim_custom /det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt /det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt /det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt/CMakeFiles/benchClient.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : CMakeFiles/benchClient.dir/depend

