# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/src/conn_handler.c" "/det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt/CMakeFiles/dim-core.dir/src/conn_handler.c.o"
  "/det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/src/copy_swap.c" "/det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt/CMakeFiles/dim-core.dir/src/copy_swap.c.o"
  "/det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/src/dic.c" "/det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt/CMakeFiles/dim-core.dir/src/dic.c.o"
  "/det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/src/dim_thr.c" "/det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt/CMakeFiles/dim-core.dir/src/dim_thr.c.o"
  "/det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/src/dis.c" "/det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt/CMakeFiles/dim-core.dir/src/dis.c.o"
  "/det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/src/dll.c" "/det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt/CMakeFiles/dim-core.dir/src/dll.c.o"
  "/det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/src/dna.c" "/det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt/CMakeFiles/dim-core.dir/src/dna.c.o"
  "/det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/src/dns.c" "/det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt/CMakeFiles/dim-core.dir/src/dns.c.o"
  "/det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/src/dtq.c" "/det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt/CMakeFiles/dim-core.dir/src/dtq.c.o"
  "/det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/src/hash.c" "/det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt/CMakeFiles/dim-core.dir/src/hash.c.o"
  "/det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/src/open_dns.c" "/det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt/CMakeFiles/dim-core.dir/src/open_dns.c.o"
  "/det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/src/sll.c" "/det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt/CMakeFiles/dim-core.dir/src/sll.c.o"
  "/det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/src/swap.c" "/det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt/CMakeFiles/dim-core.dir/src/swap.c.o"
  "/det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/src/tcpip.c" "/det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt/CMakeFiles/dim-core.dir/src/tcpip.c.o"
  "/det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/src/utilities.c" "/det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt/CMakeFiles/dim-core.dir/src/utilities.c.o"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "MIPSEL"
  "PROTOCOL=1"
  "TDAQ_PACKAGE_NAME=\"dim_custom\""
  "_GNU_SOURCE"
  "_REENTRANT"
  "__USE_XOPEN2K8"
  "dim_core_EXPORTS"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "."
  "../"
  "/cvmfs/atlas.cern.ch/repo/sw/tdaq/tdaq-common/tdaq-common-04-04-00/installed/include"
  "/cvmfs/atlas.cern.ch/repo/sw/tdaq/tdaq-common/tdaq-common-04-04-00/installed/x86_64-centos7-gcc11-opt/include"
  "/cvmfs/atlas.cern.ch/repo/sw/tdaq/tdaq-common/tdaq-common-04-04-00/installed/external/x86_64-centos7-gcc11-opt/include"
  "/cvmfs/atlas.cern.ch/repo/sw/tdaq/tdaq/tdaq-09-04-00/installed/include"
  "/cvmfs/atlas.cern.ch/repo/sw/tdaq/tdaq/tdaq-09-04-00/installed/x86_64-centos7-gcc11-opt/include"
  "/cvmfs/atlas.cern.ch/repo/sw/tdaq/tdaq/tdaq-09-04-00/installed/external/x86_64-centos7-gcc11-opt/include"
  )
set(CMAKE_DEPENDS_CHECK_CXX
  "/det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/src/diccpp.cxx" "/det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt/CMakeFiles/dim-core.dir/src/diccpp.cxx.o"
  "/det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/src/dimcpp.cxx" "/det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt/CMakeFiles/dim-core.dir/src/dimcpp.cxx.o"
  "/det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/src/discpp.cxx" "/det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt/CMakeFiles/dim-core.dir/src/discpp.cxx.o"
  "/det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/src/tokenstring.cxx" "/det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/x86_64-centos7-gcc11-opt/CMakeFiles/dim-core.dir/src/tokenstring.cxx.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "MIPSEL"
  "PROTOCOL=1"
  "TDAQ_PACKAGE_NAME=\"dim_custom\""
  "_GNU_SOURCE"
  "_REENTRANT"
  "__USE_XOPEN2K8"
  "dim_core_EXPORTS"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "."
  "../"
  "/cvmfs/atlas.cern.ch/repo/sw/tdaq/tdaq-common/tdaq-common-04-04-00/installed/include"
  "/cvmfs/atlas.cern.ch/repo/sw/tdaq/tdaq-common/tdaq-common-04-04-00/installed/x86_64-centos7-gcc11-opt/include"
  "/cvmfs/atlas.cern.ch/repo/sw/tdaq/tdaq-common/tdaq-common-04-04-00/installed/external/x86_64-centos7-gcc11-opt/include"
  "/cvmfs/atlas.cern.ch/repo/sw/tdaq/tdaq/tdaq-09-04-00/installed/include"
  "/cvmfs/atlas.cern.ch/repo/sw/tdaq/tdaq/tdaq-09-04-00/installed/x86_64-centos7-gcc11-opt/include"
  "/cvmfs/atlas.cern.ch/repo/sw/tdaq/tdaq/tdaq-09-04-00/installed/external/x86_64-centos7-gcc11-opt/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
