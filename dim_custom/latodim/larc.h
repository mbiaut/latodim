#ifndef __LARC__H__
#define __LARC__H__

#include <iostream>
#include <unistd.h>
#include <stdio.h>
#include <sstream>
#include <map>
#include <vector>
#include <fstream>
#include <string>

#include "latodim/ldpb_board.h"
#include "latodim/latome.h"

using namespace std;

class LArC : public LdpbBoard
{
    private:
        Latome m_latome1;
        Latome m_latome2;
        Latome m_latome3;
        Latome m_latome4;
        
    public:
        Latome get_latome1(){ return m_latome1; }
        void set_latome1(Latome n);

        Latome get_latome2(){ return m_latome2; }
        void set_latome2(Latome n);

        Latome get_latome3(){ return m_latome3; }
        void set_latome3(Latome n);

        Latome get_latome4(){ return m_latome4; }
        void set_latome4(Latome n);   

        string get_xmltype(){ return "LARC"; }
        //string get_register_path(){ return "/det/lar/project/firmware/LArC/LArC_FW-v2.2/firmware/projects/atlas/srcs/sources_1/sw"; }
        string get_register_path(){ return "/det/lar/project/firmware/LArC/LArC_FW-v2.2/scripts/firmware_control/xml/larc.xml"; }
};

#endif
