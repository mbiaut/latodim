#ifndef __LDPBBOARD__H__
#define __LDPBBOARD__H__

#include <iostream>
#include <unistd.h>
#include <stdio.h>
#include <sstream>
#include <map>
#include <vector>
#include <fstream>
#include <string>

#include <IpBus/IpBus.h>
#include "dim/dis.hxx"

using namespace std; //I LOVE YOU


class LdpbBoard
{
    protected:

        int m_number;
        string m_servicename;
        string m_snumber; //number et mode string pour certaine application, doive tjr etre modifié ensemble
        IpBus* m_ipbus = NULL;

    public:

        void set_servicename(string n){ m_servicename = n; }
        string get_servicename(){ return m_servicename; }

        int get_number(){ return m_number; }
        void set_number(int n);

        string get_snumber(){ return m_snumber; }

        virtual string get_xmltype(){ return "LDPB_BOARD"; }
        virtual string get_register_path(){ return "JUL"; }
        int ping();
        void setup_ipbus();
        int add_in_xml();
        string get_xml_text();
        int create_ipbus();
        const uint32_t ReadRegisterValue(const string& name);
};

#endif
