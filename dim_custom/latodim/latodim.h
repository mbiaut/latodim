#ifndef __LATODIM__H__
#define __LATODIM__H__

#include <iostream>
#include <unistd.h>
#include <stdio.h>
#include <sstream>
#include <map>
#include <vector>
#include <fstream>
#include <string>

#include <IpBus/IpBus.h>

#include "dim/dis.hxx"
#include "latodim/latodim.h"
#include "latodim/ldpb_board.h"
#include "latodim/larc.h"
#include "latodim/latome.h"

using namespace std;

int ping(string IP);
vector<LArC> get_LarC_and_Latome_list();
int create_ipbus_xml();
int main();

#endif
