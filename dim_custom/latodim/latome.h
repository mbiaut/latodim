#ifndef __LATOME__H__
#define __LATOME__H__

#include <iostream>
#include <unistd.h>
#include <stdio.h>
#include <sstream>
#include <map>
#include <vector>
#include <fstream>
#include <string>

#include "latodim/ldpb_board.h"

using namespace std; //I LOVE YOU


class Latome : public LdpbBoard
{
    private:
        bool m_exist;
        string m_larc_textnumber;
        string m_larc_servicename;

    public:
        Latome();
        Latome(int n, string sname);

        //void set_exist(bool n){ m_exist = n; }
        bool exist(){ return m_exist; }

        void set_larc_textnumber(string n){ m_larc_textnumber = n; }
        string get_larc_textnumber(){ return m_larc_textnumber; }

        void set_larc_servicename(string n){ m_larc_servicename = n; }
        string get_larc_servicename(){ return m_larc_servicename; }

        //return for dim service
        uint32_t checkfirmware();
        int checkbitslip(uint32_t* bitslip_err);

        string get_xmltype(){ return "LATOME"; }
        //string get_register_path(){ return "/det/lar/project/firmware/LATOME/LATOME_FW-v4.2.0/LATOME/release/fw-48ch-48ch/compilation/latome_hw/release/registers/registers.xml"; }
        string get_register_path(){ return "/det/lar/project/firmware/LATOME/LATOME_FW-v4.3.0/LATOME/release/fw-48ch-48ch/compilation/latome_hw/release/registers/registers.xml"; }
        string get_textnumber();
};

#endif
