if [ -z ${DIM_CUSTOMROOT+x} ]
then
  DIM_CUSTOMROOT=/det/lar/usersarea/mbiaut/devarea/latodim/dim_custom; export DIM_CUSTOMROOT
fi

if [ -z ${BASEROOT+x} ]
then
  BASEROOT=/det/lar/project/LargOnline/releases/LargOnline-940-47-00/Base/v16r4; export BASEROOT
fi

if [ -z ${IPBUSROOT+x} ]
then
  IPBUSROOT=/det/lar/project/LargOnline/releases/LargOnline-940-47-00/IpBus/v1r7; export IPBUSROOT
fi

if [ -e /det/lar/project/LargOnline/releases/LargOnline-940-47-00/Base/v16r4/setup.sh ]
then
  . /det/lar/project/LargOnline/releases/LargOnline-940-47-00/Base/v16r4/setup.sh
fi

if [ -e /det/lar/project/LargOnline/releases/LargOnline-940-47-00/IpBus/v1r7/setup.sh ]
then
  . /det/lar/project/LargOnline/releases/LargOnline-940-47-00/IpBus/v1r7/setup.sh
fi

case $SITE in
  CERN)
    DIM_DNS_NODE="`[ -f /det/lar/project/scripts/misc/larMachines.py ] && grep EMF-DNS /det/lar/project/scripts/misc/larMachines.py | cut -d "'" -f2`"; export DIM_DNS_NODE
    ;;
  POINT1)
    DIM_DNS_NODE="`[ -f /det/lar/project/scripts/misc/larMachines.py ] && grep LAR-DNS /det/lar/project/scripts/misc/larMachines.py | cut -d "'" -f2`"; export DIM_DNS_NODE
    ;;
  LAPP)
    DIM_DNS_NODE="lappc-f562.in2p3.fr"; export DIM_DNS_NODE
    ;;
  *)
    DIM_DNS_NODE="unknown.domain"; export DIM_DNS_NODE
    ;;
esac


DIM_DNS_PORT="2505"; export DIM_DNS_PORT

lar_path_prepend PATH "/det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/${CMTCONFIG}/bin"
