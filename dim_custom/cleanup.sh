
lar_path_remove PATH "/det/lar/usersarea/mbiaut/devarea/latodim/dim_custom/${CMTCONFIG}/bin"

### Sourcing cleanup script of package dependencies ###

if [ -f /det/lar/project/LargOnline/releases/LargOnline-940-47-00/Base/v16r4/cleanup.sh ]
then
  . /det/lar/project/LargOnline/releases/LargOnline-940-47-00/Base/v16r4/cleanup.sh
fi

if [ -f /det/lar/project/LargOnline/releases/LargOnline-940-47-00/IpBus/v1r7/cleanup.sh ]
then
  . /det/lar/project/LargOnline/releases/LargOnline-940-47-00/IpBus/v1r7/cleanup.sh
fi


### Unset environment variables ###

unset DIM_CUSTOMROOT
unset DIM_DNS_NODE
unset DIM_DNS_PORT
