## LATODIM
DIM server for the monitoring of the Latome and LArC specific register 

## Description
Latodim is a server for the monitoring of the Latome. It read register from the Latome or the LArC and publish by DimService this value.
The goal of Latodim is to interface it with a WinCCOA panel in the way to get a DCS monitoring programm

## Visuals
A beutiful logo to do

## Installation
Latodim must be install on a CENTOS computer, on a network with latome to monitor.
On this network you must be able to ping the latome and ask the SQL server of the phase I topologie
You must be able to use this two command :

- `ping aamlar-latome-016` (016 is for the latome 16 at emf, chose a latome on your network)
- `mysql -h dbod-lar-phase-1-be-topology-db.cern.ch --port 5506 -u reader  --database LArPhase1Topo`

To install latodim, clone (with git clone) this git on the computer you have chose, it's already compiled

## Usage (for user)
On the top directory you can find all the script or binary for the normal use of latodim :
You can use the following command :

- **setup.sh** the script to setup the bash before executing other script/binary
- **compile.sh** if you want to recompile
- **dns_dim** dns server (when you start a dim server, you must start before the dns server, it's the fonctionnement of DIM)
- **did** did is the debug client for dim server, it provide a graphical interface. With it you can visualize the service provided by the server
- **latodim** to launch latodim
- **status.sH** to see what process is launched, it give the PID (usefull if you want to kill it)
- **dim_custom** if you want to see the innards of the project go into this directory
- **gitcompile** **please don't use it** it's a script for devellopment (it compile code and put push the new binary)

The classic procedure is : 
- `source setup.sh`
- `./dns_dim &`
- `./did &`
- `./latodim &`
- `source status.sh`

## Support
If you have problem contact the developper biaut@cppm.in2p3.fr

## DIM
DIM is a library to create monitoring server (or client). It the main library used for this project

*DIM homepage* : https://dim.web.cern.ch/

## Authors and acknowledgment
Latodim authors : Mathieu Biaut

With help of : 
    
    - Clement Camincher
    
    - Clara Gaspar
    
    - Nikiforo Nikiforos

## License
There is a license for DIM I dont read it because it's to long, but it seem to be a GPL license, so I think this project is also under this license

## Project status
On developement, not finish, just the ping of the latome is publish
