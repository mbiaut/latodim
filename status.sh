#!/bin/bash

DIR=$(pwd)

#---------------------------------------------------------
# dns
#---------------------------------------------------------

SERVICE="dns_dim"
PROGRAM_NAME="dns_dim"
PID=`ps -e | sed -n /${SERVICE}/p | grep -v "logs" | awk '{ print $1 }'`
now=$(date +"%d-%m-%y-%H-%M")

echo "-------------------------------------------"
if [ "${PID:-null}" = null ];then
  echo -e "$PROGRAM_NAME :\033[31m OFF \033[0m"
  echo "-------------------------------------------"
else
  echo -e "$PROGRAM_NAME :\033[32m ON \033[0m"
  echo "-------------------------------------------"
  upTime=$(ps -eo pid,lstart,cmd|grep -v grep | grep $PID | awk '{print $3 " " $4 " " $5 " " $6}')
  echo "Uptime: $upTime"
  echo "PID: $(ps -ef |grep -v grep | grep $SERVICE | awk '{ print $2 }')"
fi
echo

#---------------------------------------------------------
# latodim
#---------------------------------------------------------

SERVICE="latodim"
PROGRAM_NAME="latodim"
PID=`ps -e | sed -n /${SERVICE}/p | grep -v "logs" | awk '{ print $1 }'`
now=$(date +"%d-%m-%y-%H-%M")

echo "-------------------------------------------"
if [ "${PID:-null}" = null ];then
  echo -e "$PROGRAM_NAME :\033[31m OFF \033[0m"
  echo "-------------------------------------------"
else
  echo -e "$PROGRAM_NAME :\033[32m ON \033[0m"
  echo "-------------------------------------------"
  upTime=$(ps -eo pid,lstart,cmd|grep -v grep | grep $PID | awk '{print $3 " " $4 " " $5 " " $6}')
  echo "Uptime: $upTime"
  echo "PID: $(ps -ef |grep -v grep | grep $SERVICE | awk '{ print $2 }')"
fi
echo

cd $DIR
